Source: libcgi-untaint-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Dominic Hargreaves <dom@earth.li>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl,
                     libcgi-pm-perl | perl (<< 5.19),
                     libuniversal-require-perl,
                     libtest-pod-perl,
                     libtest-pod-coverage-perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcgi-untaint-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcgi-untaint-perl.git
Homepage: https://metacpan.org/release/CGI-Untaint

Package: libcgi-untaint-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libuniversal-require-perl
Recommends: libcgi-pm-perl | perl (<< 5.19)
Description: module to process CGI input parameters
 Dealing with large web based applications with multiple forms is a
 minefield. It's often hard enough to ensure you validate all your
 input at all, without having to worry about doing it in a consistent
 manner. If any of the validation rules change, you often have to alter
 them in many different places. And, if you want to operate taint-safe,
 then you're just adding even more headaches.
 .
 CGI::Untaint provides a simple, convenient, abstracted and extensible
 manner for validating and untainting the input from web forms.
 .
 You simply create a handler with a hash of your parameters (usually
 $q->Vars), and then iterate over the fields you wish to extract,
 performing whatever validations you choose. The resulting variable is
 guaranteed not only to be valid, but also untainted.
