libcgi-untaint-perl (1.26-8) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 10 Jun 2022 15:18:28 +0100

libcgi-untaint-perl (1.26-7) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Bart Martens from Uploaders. Thanks for your work!

  [ Dominic Hargreaves ]
  * Add myself as Uploader (Closes: #702591)
  * Update debhelper compat to 9
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Mon, 21 Aug 2017 17:15:36 +0100

libcgi-untaint-perl (1.26-6) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit (build) dependency on libcgi-pm-perl.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Jun 2015 13:19:35 +0200

libcgi-untaint-perl (1.26-5) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * Switch to "3.0 (quilt)" source format.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Use debhelper 8 and dh(1) in debian/rules.
  * Drop version from perl build dependency.
  * Use metacpan URLs.
  * Improve short and long description.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 Aug 2014 18:01:29 +0200

libcgi-untaint-perl (1.26-4) unstable; urgency=low

  * Moving this package into the Debian Perl Group.
  * Fixed debhelper-but-no-misc-depends.
  * Fixed package-uses-deprecated-debhelper-compat-version.

 -- Bart Martens <bartm@debian.org>  Sat, 06 Jun 2009 18:49:40 +0200

libcgi-untaint-perl (1.26-3) unstable; urgency=low

  * debian/control: Added libtest-pod-perl and libtest-pod-coverage-perl to
    Build-Depends for the tests.
  * debian/control: Standards-Version and Homepage.

 -- Bart Martens <bartm@debian.org>  Mon, 24 Dec 2007 20:29:25 +0100

libcgi-untaint-perl (1.26-2) unstable; urgency=low

  * New maintainer, as agreed with Stephen.
  * debian/*: Repackaged with cdbs.
  * debian/watch: Updated to version 3.

 -- Bart Martens <bartm@knars.be>  Thu, 16 Nov 2006 14:57:37 +0100

libcgi-untaint-perl (1.26-1) unstable; urgency=low

  * New upstream release
  * Switched to my debian.org email address
  * debian/watch - Updated URL so it does not time out

 -- Stephen Quinney <sjq@debian.org>  Thu, 22 Sep 2005 20:45:55 +0100

libcgi-untaint-perl (1.25-1) unstable; urgency=low

  * New upstream release
  * New maintainer, closes: #289433.
  * debian/rules - replaced with my standard system. This removes the
    build-dependency on cdbs.

 -- Stephen Quinney <sjq@debian.org>  Wed, 12 Jan 2005 20:32:54 +0000

libcgi-untaint-perl (1.00-1) unstable; urgency=low

  * New upstream release.
  * Complies with policy 3.6.1.

 -- Dagfinn Ilmari Mannsaker <ilmari@ilmari.org>  Wed, 17 Sep 2003 07:41:34 +0200

libcgi-untaint-perl (0.90-2) unstable; urgency=low

  * Convert to CDBS.
    - debian/rules: rewritten from scratch
    - debian/control: Build-Depends: debhelper (>= 4.1.0), cdbs
    - debian/compat: put compat level here
  * Update maintainer address
  * Move to perl section
  * Complies with policy 3.6.0

 -- Dagfinn Ilmari Mannsaker <ilmari@ilmari.org>  Sat,  2 Aug 2003 03:58:23 +0200

libcgi-untaint-perl (0.90-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Standards-Version: 3.5.8
    - Build-Depends-Indep: perl (>= 5.8.0) instead of libtest-simple-perl
    - ASCII-fy my surname
  * debian/changelog: convert to UTF-8

 -- Dagfinn Ilmari Mannsaker <ilmari@ping.uio.no>  Wed, 12 Feb 2003 03:35:01 +0100

libcgi-untaint-perl (0.84-9) unstable; urgency=low

  * Rebuilt for sourceless upload.

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Wed, 14 Aug 2002 00:55:36 +0200

libcgi-untaint-perl (0.84-8) unstable; urgency=low

  * Add Build-Depends-Indep: libuniversal-require-perl.
  * Apply self-lart.

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Fri,  9 Aug 2002 18:21:18 +0200

libcgi-untaint-perl (0.84-7) unstable; urgency=low

  * add Build-Depends-Indep: libtest-simple-perl

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Fri,  9 Aug 2002 16:23:06 +0200

libcgi-untaint-perl (0.84-6) unstable; urgency=low

  * Fix typo in debian/copyright

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Fri,  9 Aug 2002 01:19:29 +0200

libcgi-untaint-perl (0.84-5) unstable; urgency=low

  * Really fix debian/copyright

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Fri,  9 Aug 2002 00:49:21 +0200

libcgi-untaint-perl (0.84-4) unstable; urgency=low

  * debian/copyright: Add pointers to full license texts.

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Wed, 24 Jul 2002 01:24:56 +0200

libcgi-untaint-perl (0.84-3) unstable; urgency=low

  * Close ITP.  (Closes: #153137)
  * Removed pointless README.Debian
  * debian/changelog: Removed "Local Variables" section.
  * debian/rules: Removed cruft from dh-make

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Tue, 16 Jul 2002 15:11:02 +0200

libcgi-untaint-perl (0.84-2) unstable; urgency=low

  * Added Depends: libuniversal-require-perl

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Tue, 16 Jul 2002 13:04:49 +0200

libcgi-untaint-perl (0.84-1) unstable; urgency=low

  * Initial Release.

 -- Dagfinn Ilmari Mannsåker <ilmari@ping.uio.no>  Mon, 15 Jul 2002 21:26:51 +0200
